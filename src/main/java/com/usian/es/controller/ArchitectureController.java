package com.usian.es.controller;


import com.usian.es.bean.ArchitectureDto;
import com.alibaba.fastjson.JSON;
import com.usian.es.config.RestClientConfig;
import com.usian.es.pojo.Architecture;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author: 周舟
 * @Desc:
 * @create: 2023-12-26 15:23
 **/

@RestController
@RequestMapping("/esArchitecture")
public class ArchitectureController {

    @Autowired
    RestHighLevelClient restHighLevelClient;


    @RequestMapping("/indexArchitecture")
    public String indexTest() throws IOException {

        ArchitectureDto architectureDto = new ArchitectureDto();
        architectureDto.setId("4");
        architectureDto.setName("大厦2433");
        architectureDto.setProvince("北京2333");
        architectureDto.setCity("海淀区2333");
        architectureDto.setArea("西城区2333");
        architectureDto.setAddress("北京市海淀区2333");
        architectureDto.setDescription("大厦2333");
        architectureDto.setScore(100);
        architectureDto.setPrice(100000000);





        // 创建好index请求
        IndexRequest indexRequest = new IndexRequest("architecture_index");
        // 设置索引
        indexRequest.id("4");
        // 设置超时时间（默认）
        indexRequest.timeout(TimeValue.timeValueSeconds(5));


        indexRequest.source(JSON.toJSONString(architectureDto), XContentType.JSON);

        //执行添加请求
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(indexResponse);

        return "success";

    }
}
