package com.usian.es.controller;

import com.alibaba.fastjson.JSON;
import com.usian.es.bean.Arcchaaa;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author: wuzhenqiang
 * @Desc:
 * @create: 2023-12-26 15:24
 **/
@RestController
@RequestMapping("/as")
public class AsController {
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @RequestMapping("indexTest")
    public String indexTest() throws IOException  {
        Arcchaaa arcchaaa = new Arcchaaa();
        arcchaaa.setAname("安徽省");
        arcchaaa.setStaty("合肥");
        arcchaaa.setXingl("合肥2");
        IndexRequest indexRequest = new IndexRequest("architecture_index");
        indexRequest.source(JSON.toJSONString(arcchaaa), XContentType.JSON);
        //执行添加请求
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(indexResponse);
        return "success";
    }
}
