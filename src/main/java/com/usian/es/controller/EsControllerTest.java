package com.usian.es.controller;

import com.alibaba.fastjson.JSON;
import com.usian.es.bean.ArchitectureDto;
import com.usian.es.bean.UserDto;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

public class EsControllerTest {
    @Autowired
    RestHighLevelClient restHighLevelClient;


    /**
     * 从mysql查询
     * 同步es
     * 1：全量同步（初始化） 100W  项目启动的时候
     * 2：增量同步        数据发生改变的时候
     * 3：定时同步 XXL-JOB  凌晨1点执行一次  多线程
     * 4: 异步同步  异步任务
     * @return
     * @throws IOException
     */
    @RequestMapping("/index")
    public String indexTest() throws IOException {

        ArchitectureDto architectureDto = new ArchitectureDto();
        architectureDto.setId("666");
        architectureDto.setName("李强");
        architectureDto.setAddress("北京市朝阳区");

        // 创建好index请求
        IndexRequest indexRequest = new IndexRequest("architecture_index");
        // 设置索引
        indexRequest.id("10");
        // 设置超时时间（默认）
        indexRequest.timeout(TimeValue.timeValueSeconds(5));
        indexRequest.source(JSON.toJSONString(architectureDto), XContentType.JSON);
        //执行添加请求
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(indexResponse);

        return "success";

    }
    @RequestMapping("/user")
    public String userTest() throws IOException {

        UserDto userDto = new UserDto();
        userDto.setId("1");
        userDto.setName("小明");
        userDto.setAge("18");
        // 创建好index请求
        IndexRequest indexRequest = new IndexRequest("architecture_index");
        // 设置索引
//        indexRequest.id("5");
        // 设置超时时间（默认）
        indexRequest.timeout(TimeValue.timeValueSeconds(5));
        indexRequest.source(JSON.toJSONString(userDto), XContentType.JSON);
        //执行添加请求
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(indexResponse);

        return "success";

    }
}
