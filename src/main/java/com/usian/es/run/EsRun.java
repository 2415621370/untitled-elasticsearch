package com.usian.es.run;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @TODO:
 * @Author:baimu
 * @Date:2023 12 26 10 02
 **/

@Component
public class EsRun {

    @PostConstruct
    public void run(){
        System.out.println("EsRun run");
    }
}
