package com.usian.es.run;

import com.alibaba.fastjson.JSON;
import com.usian.es.bean.ArchitectureDto;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @TODO:
 * @Author:baimu
 * @Date:2023 12 26 09 57
 **/
@Component
@Slf4j
public class EsDataRun implements CommandLineRunner {
    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    public void run(String... args) throws Exception {

    }
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        log.info("项目启动中---------");
//        ArchitectureDto architectureDto = new ArchitectureDto();
//        architectureDto.setId("51");
//        architectureDto.setName("大厦111");
//        architectureDto.setProvince("北京111");
//        architectureDto.setCity("海淀区");
//        architectureDto.setArea("西城区");
//        architectureDto.setAddress("北京市海淀区");
//        architectureDto.setDescription("大厦");
//        architectureDto.setScore(100);
//        architectureDto.setPrice(100000000);
//
//
//
//
//
//        // 创建好index请求
//        IndexRequest indexRequest = new IndexRequest("architecture_index");
//        // 设置索引
//        indexRequest.id("5");
//        // 设置超时时间（默认）
//        indexRequest.timeout(TimeValue.timeValueSeconds(5));
//
//
//        indexRequest.source(JSON.toJSONString(architectureDto), XContentType.JSON);
//
//        //执行添加请求
//        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
//        System.out.println(indexResponse);
//    }
}
