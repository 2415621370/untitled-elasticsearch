package com.usian.es.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author: wuzhenqiang
 * @Desc:
 * @create: 2023-12-26 15:22
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Arcchaaa {
    public Integer id;
    @Field(type = FieldType.Text)
    private String aname;
    @Field(type = FieldType.Text)
    private String staty;
    @Field(type = FieldType.Text)
    private String xingl;
}
