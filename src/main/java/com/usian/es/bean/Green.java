
package com.usian.es.bean;

import lombok.Data;

@Data
public class Green {
    private Integer q;
    private Integer e;
    private String o;
    private String red;
    private String blue;
    private String yellow;
    private String black;
    private String white;


}