package com.usian.es.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @TODO:
 * @Author:baimu
 * @Date:2023 12 26 09 07
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArchitectureDto {


    @Id
    private String id;


    @Field(type = FieldType.Text)
    private String name;


    @Field(type = FieldType.Text)
    private String province;


    @Field(type = FieldType.Text)
    private String city;


    @Field(type = FieldType.Text)
    private String area;


    @Field(type = FieldType.Text)
    private String address;





    @Field(type = FieldType.Text)
    private String description;


    @Field(type = FieldType.Double)
    private double score;


    @Field(type = FieldType.Double)
    private double price;
}
