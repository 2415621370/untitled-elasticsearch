package com.usian.es.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

/**
 * @TODO:
 * @Author:baimu
 * @Date:2023 12 26 09 01
 **/

@Configuration
public class RestClientConfig extends AbstractElasticsearchConfiguration {



    @Value("${es.servers}")
    private String servers;

    @Value("${es.hort}")
    private int port;


    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo(servers + ":" + port)
                .build();
        return RestClients.create(clientConfiguration).rest();
    }
}
