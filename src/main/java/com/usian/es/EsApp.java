package com.usian.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @TODO:
 * @Author:baimu
 * @Date:2023 12 26 09 12
 **/
@SpringBootApplication
public class EsApp {
    public static void main(String[] args) {
        SpringApplication.run(EsApp.class,args);
    }
}
